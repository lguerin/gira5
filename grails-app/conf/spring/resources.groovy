import gira.listeners.GiraEventsListener
import gira.monitors.AuthHealthMonitor

// Place your Spring DSL code here
beans = {

    // Register GiraEventsListener as Spring bean
    giraEventsListener(GiraEventsListener) {
        notificationService = ref('notificationService')
    }

    // Auth backend monitor
    authHealthMonitor(AuthHealthMonitor)

}

