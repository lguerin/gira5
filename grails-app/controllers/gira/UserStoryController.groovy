package gira

import gira.constants.MetricsConstant
import gira.types.GiraEventType
import grails.plugin.dropwizard.metrics.meters.Metered
import grails.plugin.dropwizard.metrics.timers.Timed
import grails.rest.RestfulController

import static org.springframework.http.HttpStatus.BAD_REQUEST
import static org.springframework.http.HttpStatus.NO_CONTENT

class UserStoryController extends RestfulController {

    static responseFormats = ['json', 'xml']

    static allowedMethods = [last: "GET", followers: "GET", storyPoints: "PUT"]

    UserStoryDataService userStoryDataService
    UserStoryTxService userStoryTxService
    // NotificationService notificationService
    GiraBusService giraBusService

    UserStoryController() {
        super(UserStory)
    }

    /**
     * Get last created user stories ordered by title desc
     * @param max   Max number of UserStory to return
     * @return
     */
    List<UserStory> last(int max) {
        List<UserStory> lastUserStories = userStoryDataService.findUserStories([max: max, sort: 'title', order: 'desc'])
        respond lastUserStories
    }

    /**
     * Get UserStory followers
     *
     * @return
     */
    @Metered(MetricsConstant.FOLLOWERS)
    List<User> followers() {
        UserStory userStory = userStoryDataService.getUserStory(params.id)
        Set<User> followers = []
        if (userStory) {
            followers = userStory.followers
        }
        respond followers
    }

    /**
     * Update the story points of a given UserStory
     */
    def storyPoints() {
        boolean success = userStoryDataService.updateStoryPoints(params.id, params.points as int)
        render status: success ? NO_CONTENT : BAD_REQUEST
    }

    /**
     * Update the story points of a given UserStory with some latency in order to look at
     * lock and concurrency behavior.
     */
    def storyPointsWithLantency() {
        boolean success = userStoryTxService.updateStoryPointsFromTx2(params.id, params.points as int, params.latency as int)
        render status: success ? NO_CONTENT : BAD_REQUEST
    }

    @Timed(MetricsConstant.SHOW)
    @Override
    Object show() {
        // Transaction tests
        // userStoryDataService.updateTestTransaction(params.id)

        // Exceptions and detached objects
        // UserStory us = UserStory.get(params.id)
        // try {
        //     userStoryDataService.updateTestTransaction(params.id, -5)
        // }
        // catch (e) {
        //     UserStory newUs = UserStory.get(params.id)
        //     log.info "Number of followers: " + newUs.followers?.size()
        // }
        return super.show()
    }

    @Override
    protected UserStory saveResource(Object resource) {
        Long start = System.currentTimeMillis()
        UserStory us = super.saveResource(resource)
        log.info "Save a user story"

        // Synchronous chain of services
        // notificationService.sendEmail(us.createdBy)

        // Asynchronous chain of services
        giraBusService.triggerUserStoryEvent(GiraEventType.USER_STORY_CREATED, us)

        Long end = System.currentTimeMillis()
        log.info "Save a user story took: ${end - start} ms."
        return us
    }
}
