
    import gira.UserStory

    model {
        Iterable<UserStory> userStoryList
    }

    xmlDeclaration()

    userStories {
        userStoryList.each {
            userStory(title: it.title, description: it.description)
        }
    }

