package gira

import gira.events.UserStoryEvent
import gira.types.GiraEventType
import grails.events.EventPublisher

class GiraBusService implements EventPublisher {

    /**
     * Trigger UserStory async events
     *
     * @param type  Event type
     * @param us    User story
     */
    void triggerUserStoryEvent(GiraEventType type, UserStory us) {
        UserStoryEvent event = new UserStoryEvent(id: us.id, dateTime: new Date())
        if (type == GiraEventType.USER_STORY_CREATED) {
            this.notify(GiraEventType.USER_STORY_CREATED.name, event)
        }
    }

}
