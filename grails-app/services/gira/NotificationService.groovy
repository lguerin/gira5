package gira

import grails.gorm.transactions.Transactional

@Transactional
class NotificationService {

    def sendEmail(int userStoryId) {
        Thread.sleep(5000)
        UserStory us = UserStory.get(userStoryId)
        log.info "Email sent to: " + us.createdBy.dump()
    }
}
