package gira

class Fixtures {

    /**
     * User builder for tests
     *
     * @param params
     * @param toSave
     * @return
     */
    static User buildUser(params = [:], toSave = true) {
        def login = "login_${UUID.randomUUID()}"
        params = [
                login   : params.login ?: login,
                fullname: params.fullname ?: 'Guillaume Laforge',
                email   : params.email ?: "${login}@ifap.nc"
        ]
        return toSave ? new User(params).save(flush: true) : new User(params)
    }


    /**
     * UserStory builder for tests
     *
     * @param params
     * @param toSave
     * @return
     */
    static UserStory buildUserStory(params = [:], toSave = true) {
        def title = "us_${UUID.randomUUID()}"
        params = [
            title: params.title ?: title,
            description: params.description ?: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
            createdBy: params.createdBy ?: Fixtures.buildUser(),
            assignedTo: params.assignedTo ?: Fixtures.buildUser(),
            comments: [],
            followers: [],
            storyPoints: params.storyPoints ?: 15
        ]
        return toSave ? new UserStory(params).save(flush: true) : new UserStory(params)
    }

    /**
     * UserStory with comments builder for tests
     *
     * @param params
     * @param toSave
     * @return
     */
    static UserStory buildUserStoryWithComments(params = [:], toSave = true) {
        UserStory us1 = Fixtures.buildUserStory(params)
        us1.addToComments(Fixtures.buildComment([:], false))
        us1.addToComments(Fixtures.buildComment([:], false))
        us1.save(flush: toSave)
    }

    /**
     * Comment builder for tests
     *
     * @param params
     * @param toSave
     * @return
     */
    static Comment buildComment(params = [:], toSave = true) {
        def description = "comment_${UUID.randomUUID()}"
        params = [
                description: params.description ?: description,
                createdBy: Fixtures.buildUser(),
        ]
        return toSave ? new Comment(params).save(flush: true) : new Comment(params)
    }
}