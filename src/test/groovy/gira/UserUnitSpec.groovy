package gira

import grails.testing.gorm.DomainUnitTest
import spock.lang.Specification
import spock.lang.Unroll

class UserUnitSpec extends Specification implements DomainUnitTest<User> {

    void "should persist user instances"() {
        setup:
        Fixtures.buildUser()
        Fixtures.buildUser()

        expect:
        User.count() == 2
    }

    void "should get user instance by login"() {
        setup:
        Fixtures.buildUser([login: 'user1', fullname: 'User 1', email: 'user1@ifap.nc'])

        when:
        User user = User.findByLogin('user1')

        then:
        user.login != null
        user.fullname == 'User 1'
        user.email == 'user1@ifap.nc'
        user.validate()
    }

    @Unroll('test User.validate() with email: #email should have returned #valid with errorCode: #errorCode')
    void "should get valid user email"() {
        setup:
        User user = Fixtures.buildUser()

        when:
        user.email = email

        then:
        valid == user.validate()
        errorCode == user.errors['email']?.code

        where:
        email                  |  valid   | errorCode
        null                   |  true    | null
        ''                     |  false   | 'blank'
        'user1@ifap.nc'        |  true    | null
        'user2'                |  false   | 'email.invalid'
    }
}
