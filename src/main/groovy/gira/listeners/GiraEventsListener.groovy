package gira.listeners

import gira.NotificationService
import gira.events.UserStoryEvent
import grails.events.annotation.Events
import grails.events.annotation.Subscriber
import groovy.util.logging.Slf4j
import org.grails.datastore.mapping.engine.event.PreInsertEvent

@Slf4j
@Events(namespace = 'gira')
class GiraEventsListener {

    NotificationService notificationService

    @Subscriber
    void onUserStoryCreated(UserStoryEvent event) {
        log.info("Get new UserStoryEvent event: " + event.dump())
        notificationService.sendEmail(event.id)
    }

}
