package gira.types

enum GiraEventType {

    USER_STORY_CREATED('gira:userStoryCreated')

    String name;

    GiraEventType(String name) {
        this.name = name
    }
}