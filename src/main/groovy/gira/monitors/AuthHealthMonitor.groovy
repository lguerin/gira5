package gira.monitors

import org.springframework.boot.actuate.health.Health
import org.springframework.boot.actuate.health.HealthIndicator

class AuthHealthMonitor implements HealthIndicator {

    @Override
    Health health() {
        if (isAuthBackendUp()) {
            return Health.up().build()
        }
        return Health.down().withDetail("Auth backend status: ", "Unreachable").build();
    }

    private  boolean isAuthBackendUp() {
        // Simulate Auth backend health
        return true
    }

}
